#!/usr/bin/env ruby 
#encoding: utf-8 
require 'spec_helper'
require './lib/bibliography/gem/list.rb'
require './lib/bibliography/gem/bibliography.rb'

describe List do
    before :each do
      
        
            @p1 = Bibliography::Book.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"], "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers' Guide", "(The Facets of Ruby)", "Pragmatic Bookshelf;", "4 edition", "July 7, 2013", ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"]) 
            @node1 = Node1.new(@p1,nil)
            	    
            @p2 = Bibliography::Book.new(["Scott Chacon"], "Pro Git 2009th Edition.", "(Pro).", "Apress;", "2009 edition", "(August 27, 2009).", ["ISBN-13: 978- 1430218333.", "ISBN-10: 1430218339."])
            @node2 = Node1.new(@p2,nil)
                          
            @p3 = Bibliography::Book.new(["David Flanagan", "Yukihiro Matsumoto"], "The Ruby Programming Language.", 0, "OReilly Media;", "1 edition", "(February 4, 2008).", ["ISBN-10: 0596516177.", "ISBN-13: 978-0596516178."]) 
            @node3 = Node1.new(@p3,nil)
                          
        	@p4 = Bibliography::Book.new(["David Chelimsky", "Dave Astels", "Bryan Helmkamp", "Dan North", "Zach Dennis", "Aslak Hellesoy."],"The RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends", "(The Facets of Ruby).", "Pragmatic Bookshelf;", "1 edition", "(December 25, 2010).", ["ISBN-10: 1934356379.", "ISBN-13: 978-1934356371."]) 
            @node4 = Node1.new(@p4,nil)
                          
            @p5 = Bibliography::Book.new(["Richard E."], "Silverman Git Pocket Guide", 0, "OReilly Media;", "1 edition", "(August 2, 2013).", ["ISBN-10: 1449325866.", "ISBN-13: 978-1449325862."]) 
            @node5 = Node1.new(@p5,nil)
	end
    
    describe "Pruebas para List" do
    	it "Se extrae el primer elemento de la lista" do
            @list = List.new(nil)
			@list.push(@node5)
			expect(@list.pop()).to eq(@node5)
		end
    end

        it  "Se puede insertar un elemento" do
    		@list = List.new(nil)
    		@list.push(@node1)
        end

	   it  "Se pueden insertar varios elementos" do
			@list = List.new(nil)
			@list.push(@node1)
			@list.push(@node2)
       	end
       	
        it  "Se pueden extraer varios elementos" do
			@list = List.new(nil)
			@list.push(@node1)
			@list.push(@node2)
            expect(@list.pop()).to eq(@node2)
            expect(@list.pop()).to eq(@node1)
        end 
        
        it  "Debe existir una Lista con su cabeza" do
        	@list = List.new(nil)
        	@list.push(@node4)
        	@list.push(@node3)
            @list.push(@node1)
            expect(@list.head).to eq(@node1)
       	end
        
        it "La lista está vacía" do
            @list = List.new(nil)
            expect(@list.empty?).to eq(true) 
        end
end