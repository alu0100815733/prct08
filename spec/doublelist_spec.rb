#!/usr/bin/env ruby 
#encoding: utf-8 
require 'spec_helper'
require './lib/bibliography/gem/doublelist.rb'

describe DoubleList do
    before :each do
        @l1 = DoubleList.new(nil, nil)
        @l1.insert_inicio(1)
        @l1.insert_final(2)
        @l1.insert_final(3)
    end
    
    describe "Prueba para Node" do
       
        it 'Debe existir un Nodo de la lista con sus datos y su siguiente' do
            expect(@l1.head.get_value != nil && @l1.head.get_nxt != nil)
        end
        
        
        it "Existe un nodo con un valor" do
            @n1 = Node.new(1,nil,nil)
            expect(@n1.get_value()).to eq(1)
        end
        
        it "Existe un nodo con un siguiente" do
            @n1 = Node.new(1,nil,nil)
            @n2 = Node.new(1,@n1,nil)
            expect(@n2.get_nxt()).to eq(@n1)
        end
        
        it "Existe un nodo con su anterior" do
            @n1 = Node.new(1,nil,nil)
            @n2 = Node.new(1,nil,@n1)
            expect(@n2.get_prev()).to eq(@n1)
        end
    end

    describe "Pruebas para DoubleList" do
        
        it "Se puede extraer elementos de la lista" do
            @l1.del_inic
            @l1.del_inic
            expect(@l1.length()).to eq(1)
        end
    
    
        it  "Se puede insertar un elemento" do
            @l1.insert_final(4)
        	expect(@l1.length()).to eq(4)
        end
        
        
        it 'Se pueden insertar varios elementos' do
            @l1.insert_final(5)
            @l1.insert_final(6)
            expect(@l1.length()).to eq(5)
        end
        
        it 'Se pueden insertar elementos al final de la lista' do
            @l1.insert_final(5)
            @l1.insert_final(6)
            expect(@l1.length()).to eq(5)
        end
        
        it 'Se pueden insertar elementos al inicio de la lista' do
            @l1.insert_inicio(5)
            @l1.insert_inicio(6)
            expect(@l1.length()).to eq(5)
        end
        
        it "Se extraer elementos por el comienzo de la lista" do
            @l1.del_inic
            @l1.del_inic
            expect(@l1.length()).to eq(1)
        end
        
        
        it "Se extraer elementos por el final de la lista" do
            @l1.del_fin
            @l1.del_fin
            expect(@l1.length()).to eq(1)
        end
    end
end