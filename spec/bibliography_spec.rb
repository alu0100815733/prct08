#!/usr/bin/env ruby 
#encoding: utf-8
require 'spec_helper'
require './lib/bibliography/gem/list.rb'
require './lib/bibliography/gem/bibliography.rb'

describe Bibliography do
  before :each do
    @b1 = Bibliography::Book.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"], "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide", "(The Facets of Ruby)", "Pragmatic Bookshelf", "4 edition", "July 7, 2013", ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"])
    @p1 = Bibliography::Periodicals.new("Thejournal" , "Thenewspapper", nil)

  end 
    
  describe "Pruebas para la clase Book" do
   
    it "Existen uno o más autores" do
      @b1.author.should eq(["Dave Thomas", "Andy Hunt", "Chad Fowler"]) 
    end
  
  
    it "Existe un título" do
      @b1.title.should eq("Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide") 
    end

   
    it "Existe una serie" do
      @b1.series.should eq("(The Facets of Ruby)") 
    end
  
  
    it "Existe una editorial" do
      @b1.editorial.should eq("Pragmatic Bookshelf") 
    end
  
  
    it "Existe un número de edición" do
      @b1.edition_number.should eq("4 edition") 
    end
  
  
    it "Existe una fecha de publicación" do
      @b1.date.should eq("July 7, 2013") 
    end
  
  
    it "Existe uno o mas números ISBN" do
      @b1.isbn.should eq(["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"]) 
    end
  
  
    it "Existe un método para obtener el listado de autores" do
      @b1.get_author().should eq(["Dave Thomas", "Andy Hunt", "Chad Fowler"]) 
    end
  
  
    it "Existe un método para obtener el título" do
      @b1.get_title().should eq("Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide") 
    end
  
  
    it "Existe un método para obtener la serie" do
      @b1.get_series().should eq("(The Facets of Ruby)") 
    end
  
  
    it "Existe un método para obtener la editorial" do
      @b1.get_editorial().should eq("Pragmatic Bookshelf") 
    end
  
  
    it "Existe un método para obtener el número de edición" do
      @b1.get_edition_number().should eq("4 edition") 
    end
  
  
    it "Existe un método para obtener la fecha de publicación" do
      @b1.get_date().should eq("July 7, 2013") 
    end
  
  
    it "Existe un método para obtener el listado de ISBN" do
      @b1.get_isbn().should eq(["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"]) 
    end
  
  
    it "Existe un método para obtener la referencia formateada" do
      @b1.formatted_reference().should eq("AUTORES: Dave Thomas, Andy Hunt, Chad Fowler | TÍTULO: Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide | SERIE: (The Facets of Ruby) | EDITORIAL: Pragmatic Bookshelf | NÚMERO DE EDICIÓN: 4 edition | FECHA DE PUBLICACIÓN: July 7, 2013 | ISBN: ISBN-13: 978-1937785499, ISBN-10: 1937785491") 
    end

  end 
  
  describe "Pruebas para la clase Periodicals" do
  
    it "Existe un método para obtener el artículo de revista" do
      @p1.get_journal().should eq("Thejournal") 
    end
  
  
    it "Existe un método para obtener el periódico" do
      @p1.get_newspapper().should eq("Thenewspapper") 
    end
  
  
    it "Existe un método para obtener un documento electrónico" do
      @p1.get_elecdocument().should eq(nil) 
    end

  end
  
  describe "Pruebas para el módulo Bibliography" do
      
    it "Comprobar herencia de la clase Periodicals" do
      expect(@p1.is_a? Bibliography::Book).to eq(true)
    end
    
    it "Comprobar instancia de Periodicals" do
      expect(@p1).to be_an_instance_of (Bibliography::Periodicals)
    end
   
  end

end



