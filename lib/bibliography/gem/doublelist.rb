Node = Struct.new(:value, :nxt, :prev) do
  # Metodo para inicializar la clase
    def initialize (value, nxt, prev)
        @value = value
        @nxt= nxt
        @prev = prev
    end
  
  # Funcion para obtener el valor de un nodo
    def get_value
        return @value
    end

  # Funcion para obtener el puntero a siguiente de un nodo
    def get_nxt
        return @nxt
    end

  # Funcion para obtener el puntero al anterior de un nodo
    def get_prev
        return @prev
    end
  
  # Metodo para cambiar el puntero a siguiente de un nodo
    def mod_nxt(nxt)
        @nxt = nxt
    end

  # Metodo para cambiar el puntero al anterior de un nodo
    def mod_prev(prev)
        @prev = prev
    end

end


class DoubleList

    attr_accessor :head, :tail
    
    def initialize(head, tail)
        @head = head
        @tail = tail
    end
    
     # Metodo para insertar un nodo al principio de la lista
    def insert_inicio(data_)
        if @head != nil && @head.get_nxt != nil
            siguiente = @head
            @head = Node.new(data_, siguiente, nil)
            siguiente.mod_prev(@head)
        elsif @head != nil
            siguiente = @head
            @head = Node.new(data_, siguiente, nil)
            siguiente.mod_prev(@head)
            @tail = siguiente
        else
            @head = Node.new(data_, nil, nil)
            @tail = @head
        end
    end
    
      # Metodo para insertar un nodo al final de la lista
    def insert_final(data_)
        if @tail != nil
            @tail = Node.new(data_, nil, @tail)
            anterior = @tail.get_prev
            anterior.mod_nxt(@tail)
         else
            @head = Node.new(data_, nil, nil)
            @tail = @head
        end
    end

  # Metodo para eliminar un nodo del principio de la lista
    def del_inic
        @head = @head.get_nxt
        if @head != nil
            @head.mod_prev(nil)
        end
    end
    
  # Metodo para eliminar un nodo del final de la lista
    def del_fin
        @tail = @tail.get_prev
        if @tail != nil
            @tail.mod_nxt(nil)
        else
            @head = @tail
        end
    end
    
    def extract_inic()
          extract=@head
          @head=@head.nxt
          return extract
    end
    
    def extract_fin()
          extract=@tail
          @tail=@tail.prev
          return extract
    end
    
    def length 
        if @head == nil
            num = 0
        else
            count = @head
            num = 1
            while count.get_nxt != nil
                num += 1
                count = count.get_nxt
            end
        end
            num
    end
end