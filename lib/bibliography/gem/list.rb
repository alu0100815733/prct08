#encodign: utf-8

        Node1 = Struct.new(:value, :next) 

class List
    
      #Creamos la clase lista con dos atributos. head:que es el comienzo de la lista.
      attr_accessor :head
      
      #En el constructor simplemente inicializamos los elementos a null.
      def initialize(head)
		  @head = head
      end
      
      #El to_s muestra desde el first hasta el final de la cola.
      def to_s
	      "#{@head} -> "
      end
      
      #El método push inserta un elemento por el principio de la lista. Dicho elemento se le pasa.
      def push(data_)
          data_.next=@head
         @head=data_
      end
      
      #El método pop extrae un elemennto lista.
      def pop()
          extract=@head
          @head=@head.next
          return extract
      end
      
      #Método que dice si la lista está vacía.
      def empty?
          @head==nil
      end
end